//
//  InsertViewController.swift
//  vildtudbytte-mvp
//
//  Created by Jesper Leth on 16/10/2018.
//  Copyright © 2018 Jesper Leth. All rights reserved.
//

import UIKit
import RealmSwift


class InsertViewController: UIViewController {

    var incomingTur: Ture? = nil
    let realm = try! Realm()

    
    
    @IBOutlet weak var turTextField: UITextField!
    @IBOutlet weak var turSwitch: UISwitch!
    @IBAction func saveButtonAction(_ sender: Any) {
        
        if let goodTur = incomingTur {
            try! realm.write {
                goodTur.IsDone = turSwitch.isOn
                goodTur.TurText = turTextField.text!
            }
        }
        else {
            let tur = Ture()
            tur.TurText = turTextField.text!
            tur.IsDone = turSwitch.isOn
        
            try! realm.write {
                realm.add(tur)
            }
            
        }
        
        navigationController?.popViewController(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let goodTur = incomingTur {
            turTextField.text = goodTur.TurText
            turSwitch.isOn = goodTur.IsDone
        }
        // Do any additional setup after loading the view.
    }
    

}
