//
//  ViewController.swift
//  vildtudbytte-mvp
//
//  Created by Jesper Leth on 16/10/2018.
//  Copyright © 2018 Jesper Leth. All rights reserved.
//

import UIKit
import RealmSwift


var ture: Results<Ture>!
var realm = try! Realm()

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
  
    @IBOutlet weak var turTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ture = realm.objects(Ture.self)
        turTableView.dataSource = self
        turTableView.delegate = self
        reload()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        reload()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "CellClick" {
            let destination = segue.destination as! InsertViewController
            let tur = ture[turTableView.indexPathForSelectedRow!.row]
            destination.incomingTur = tur
            
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ture.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TurCell", for: indexPath) as! TurCell
        let tur = ture[indexPath.row]
        cell.turText.text = tur.TurText
        cell.isDoneText.text = tur.IsDone ? "It is Done" : "Do it!"
        
        return cell
    }
    
    func reload() {
        turTableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            try! realm.write {
                realm.delete(ture[indexPath.row])
            }
            reload()
        }
    }
}

class Ture:Object {
    @objc dynamic var TurText = ""
    @objc dynamic var IsDone = false
}


class TurCell: UITableViewCell {
     @IBOutlet weak var turText: UILabel!
     @IBOutlet weak var isDoneText: UILabel!
    
}
